#include <iostream>

void PrintArray(int input[], const size_t size)
{
	std::cout << std::endl;

	for (int index = 0; index < size; ++index)
		std::cout << input[index] << ", ";
}

int Max(int a, int b)
{
	if (a > b) return a;
	return b;
}

int MaxValue(int input[], const size_t size)
{
	int max = 0;

	for (int index = 0; index < size; ++index)
		max = Max(max, input[index]);

	++max;  // Avoid 0 based array

	return max;
}

void CountSort(int input[], const size_t size, int exp)
{
	const size_t kBased = 10;
	int* pCount = new int[kBased];

	// Initialize to zero
	for (int index = 0; index < kBased; ++index)
		pCount[index] = 0;

	for (int index = 0; index < size; ++index)
	{
		int mapIndex = (input[index] / exp) % 10;
		++pCount[mapIndex];
	}

	// Sum previous value
	for (int index = 1; index < kBased; ++index)
		pCount[index] += pCount[index - 1];

	int* pOutput = new int[size];

	for (int index = 0; index < size; ++index)
	{
		int mapIndex = (input[index] / exp) % 10;

		pOutput[pCount[mapIndex] - 1] = input[index];
		
		--pCount[mapIndex];
	}

	// Copy over to the result.
	for (int index = 0; index < size; ++index)
		input[index] = pOutput[index];

	delete[] pCount;
	delete[] pOutput;
}

void RadixSort(int input[], const size_t size)
{
	int max = MaxValue(input, size);

	for (int exp = 1; max / exp > 0; exp *= 10)
		CountSort(input, size, exp);
}

/**
 * @brief Test counting sort.
 */
void TestCountSort()
{
	const size_t kSize = 7;

	int input[kSize]{ 1, 4, 1, 2, 7, 5, 2 };

	CountSort(input, kSize, 1);

	PrintArray(input, kSize);
}

/**
 * @brief Test radix sort.
 */
void TestRadixSort()
{
	const size_t kSize = 5;

	int input[kSize]{ 543, 986, 217, 765, 329 };

	RadixSort(input, kSize);

	PrintArray(input, kSize);
}

/**
 * @brief Program entry
 */
int main()
{
	//TestCountSort();
	TestRadixSort();

	return 0;
}
